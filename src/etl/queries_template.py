from string import Template
from src.config.config import ETL_INPUT_TABLE, GCLOUD_PROJECT, BIGQUERY_DATASET_NAME

# query bariatrica
query_bariatrica = Template(
    f"""
    -- PROCEDIMENTOS DE INTERESSE REALIZADOS NOS 6 MESES ANTERIORES À DATA DE REFERÊNCIA.
    SELECT DISTINCT idPessoa, idProcedimento
    FROM `{ETL_INPUT_TABLE}`
    WHERE
        idEmpresa = $id_empresa AND
        dtAtendimentoContaPaga < '$ref_date' AND
        dtAtendimentoContaPaga >= DATE_ADD('$ref_date', INTERVAL -6 MONTH)
    """
)

# query neonatal
query_neonatal = Template(
 f"""

    -- PROCEDIMENTOS DE INTERESSE REALIZADOS NOS 6 MESES ANTERIORES À DATA DE REFERÊNCIA.
    CREATE OR REPLACE TEMP TABLE preditivos_inicial AS
    SELECT DISTINCT idPessoa, idProcedimento
    FROM `{ETL_INPUT_TABLE}`
    WHERE
        idEmpresa = $id_empresa AND
        dtAtendimentoContaPaga < '$ref_date' AND
        dtAtendimentoContaPaga >= DATE_ADD('$ref_date', INTERVAL -6 MONTH);
    
    ------------------------------------------------------------------------------------------------------------
    
    -- GESTANTES ALTO CUSTO
    CREATE OR REPLACE TEMP TABLE gestantes_prever AS
    SELECT distinct idPessoa, AltoCusto_Gestante
    FROM `{GCLOUD_PROJECT}.{BIGQUERY_DATASET_NAME}.view_fato_classificacao_vidas`
    WHERE AltoCusto_Gestante = 1;
                
    ------------------------------------------------------------------------------------------------------------
    
    SELECT pi.idPessoa, pi.idProcedimento
    FROM preditivos_inicial AS pi
    INNER JOIN gestantes_prever AS gp
    ON pi.idPessoa = gp.idPessoa
    """
)


# query ortopedia
query_ortopedia = Template(
    f"""
    -- PROCEDIMENTOS DE INTERESSE REALIZADOS NOS 6 MESES ANTERIORES À DATA DE REFERÊNCIA.
    SELECT DISTINCT idPessoa, idProcedimento
    FROM `{ETL_INPUT_TABLE}`
    WHERE
        idEmpresa = $id_empresa AND
        dtAtendimentoContaPaga < '$ref_date' AND
        dtAtendimentoContaPaga >= DATE_ADD('$ref_date', INTERVAL -6 MONTH)
    """
)


# query bucomaxilo
query_bucomaxilo = Template(
    f"""
    -- PROCEDIMENTOS DE INTERESSE REALIZADOS NOS 6 MESES ANTERIORES À DATA DE REFERÊNCIA.
    SELECT DISTINCT idPessoa, idProcedimento
    FROM `{ETL_INPUT_TABLE}`
    WHERE
        idEmpresa = $id_empresa AND
        dtAtendimentoContaPaga < '$ref_date' AND
        dtAtendimentoContaPaga >= DATE_ADD('$ref_date', INTERVAL -6 MONTH)
    """
)

# query cardio
query_cardio = Template(
    f"""
    -- PROCEDIMENTOS DE INTERESSE REALIZADOS NOS 6 MESES ANTERIORES À DATA DE REFERÊNCIA.
    SELECT DISTINCT idPessoa, idProcedimento
    FROM `{ETL_INPUT_TABLE}`
    WHERE
        idEmpresa = $id_empresa AND
        dtAtendimentoContaPaga < '$ref_date' AND
        dtAtendimentoContaPaga >= DATE_ADD('$ref_date', INTERVAL -6 MONTH)
    """
)





