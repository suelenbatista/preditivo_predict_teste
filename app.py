from flask import Flask, request
from datetime import datetime
import os

app = Flask(__name__)


def config_runtime_environment(request):
    print(f"Chamada recebida na url: {request.url}.")

    if ("dev-preditivo-predict" in request.url) or ("localhost" in request.url):
        environment = 'dev'
    else:
        environment = 'prod'

    os.environ['RUNTIME_ENVIRONMENT'] = environment
    print(f"Ambiente de execução = {environment}")


@app.route('/', methods=['POST'])
def index():
    config_runtime_environment(request)

    # Eu sei que fazer import dentro do código é feio, mas é a única forma de eu configurar
    # o runtime environment antes de continuar com o resto do código.
    from src import predict

    request_json = request.get_json(force=True)
    predict.run(request_json)

    return f"""
    Pipeline de predict do modelo de {request_json['model']} finalizado em {datetime.now()}.
    
    "I knew exactly what to do. But in a much more real sense, I had no idea what to do." - Michael Scott.
    """

# if __name__ == "__main__":
#     app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8000)))